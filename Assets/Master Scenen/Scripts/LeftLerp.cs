﻿using UnityEngine;
using System.Collections;

public class LeftLerp : MonoBehaviour {

    public GameObject Player;

    private Vector3 startPos;
    private Vector3 leftEndPos;
    private float leftDistance = 7f;
    private float maxDistance = -2f;
    
    private bool leftStop = false;

    private float lerpTime = 1;
    private float currentLerpTime = 0;

    private bool keyHit = false;



	// Use this for initialization
	void Start ()
    {
        //startPos = Player.transform.position;
        //leftEndPos = Player.transform.position + Vector3.left * leftDistance;

	}
	
	// Update is called once per frame
	void Update ()
    {
        if (leftStop == false)
        {
            if (Input.GetKeyDown("a") && keyHit == false)
            {
                keyHit = true;
                startPos = Player.transform.position;
                leftEndPos = Player.transform.position + Vector3.left * leftDistance;
            }

            if (keyHit == true)
            {
                currentLerpTime += Time.deltaTime;
                if (currentLerpTime >= lerpTime)
                {
                    currentLerpTime = lerpTime;
                    keyHit = false;
                }

                float Perc = currentLerpTime / lerpTime;
                Player.transform.position = Vector3.Lerp(startPos, leftEndPos, Perc);
            }

            if (keyHit == false)
            {
                currentLerpTime = 0;
            }

            if (leftEndPos.x < maxDistance)
            {
                leftStop = true;
            }

        }
    }
}
