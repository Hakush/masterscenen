﻿using UnityEngine;
using System.Collections;

public class RightLerp : MonoBehaviour
{

    public GameObject Player;

    private Vector3 startPos;
    private Vector3 rightEndPos;
    private float rightDistance = 7f;
    private float maxDistance = 30f;

    private bool rightStop = false;

    private float lerpTime = 1;
    private float currentLerpTime = 0;

    private bool keyHit = false;

    // Use this for initialization
    void Start()
    {
        //startPos = Player.transform.position;
        //rightEndPos = Player.transform.position + Vector3.right * rightDistance;

    }

    // Update is called once per frame
    void Update()
    {
        if (rightStop == false)
        {

            if (Input.GetKeyDown("d") && keyHit == false)
            {
                keyHit = true;
                startPos = Player.transform.position;
                rightEndPos = Player.transform.position + Vector3.right * rightDistance;
            }

            if (keyHit == true)
            {
                currentLerpTime += Time.deltaTime;
                if (currentLerpTime >= lerpTime)
                {
                    currentLerpTime = lerpTime;
                    keyHit = false;
                }

                float Perc = currentLerpTime / lerpTime;
                Player.transform.position = Vector3.Lerp(startPos, rightEndPos, Perc);
            }

            if (keyHit == false)
            {
                currentLerpTime = 0;
            }

            if (rightEndPos.x > maxDistance)
            {
                rightStop = true;
            }
        }
    }
}